
(defun get-red (color-list)
  (cadr (assoc :red color-list)))

(defun get-green (color-list)
  (cadr (assoc :green color-list)))

(defun get-blue (color-list)
  (cadr (assoc :blue color-list)))

(defun get-point (color-list)
  (cadr (assoc :point color-list)))

(defun calc-normal-alpha (start end alpha)
  (* (- alpha start) (/ 1.0 (- end start))))
  
(defun add-point-to-color-list (color-list point &key red green blue)
  (list
   `((:red . ,red)
    (:green . ,green)
    (:blue . ,blue)
    (:point . ,point))
    color-list))

(defun add-points-to-color-list (color-list points)
  (append 
    (loop 
      for point in points
      collect
         (destructuring-bind
               (p &key red green blue)
             point 
        `((:red ,red)
          (:green ,green)
          (:blue ,blue)
          (:point ,p))))
   color-list))

(defparameter *color-list* 
  (add-points-to-color-list
   nil
  '((0.0 :red 255 :green 255 :blue 255)
     (0.1 :red 64 :green 0 :blue 64)
     (0.5 :red 0 :green 64 :blue 64)
     (1.0 :red 64 :green 63 :blue 0))))

(defun sort-color-list (color-list)
       (sort color-list
             #'(lambda (x y)
                 (< (cadr (assoc :point x))
                    (cadr (assoc :point y))))))

(defun make-color-list-interpolator (color-list)
  (let 
      ((sorted-color-list (sort-color-list color-list)))
    (lambda (alpha)
      (destructuring-bind
            (start end)
          (loop
             for next-color in sorted-color-list
             and color = nil then next-color
             when (<= alpha (cadr (assoc :point next-color)))
             return
               (list color next-color))
        (list
         (cons
          :red 
          ((cadr (assoc :red start))
           + (*
              (- (cadr (assoc :red end))
                 (cadr (assoc :red end))
))
  
  (format t "~A ~A~%~A~%" color next-color
                   (>= alpha (cadr (assoc :point next-color))))))))


         return (cons color next-color)))))
                 
;; to do -- 
;; sort-color-list
;; make-color-list-interpolator