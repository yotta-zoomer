;;;; cl-fractal-zoom.asd

(defpackage #:yotta-zoomer-system
  (:use #:asdf #:cl))

(in-package #:yotta-zoomer-system)

(asdf:defsystem #:yotta-zoomer
  :depends-on (:cl-glfw 
               :cl-glfw-glu 
               :cl-glfw-opengl 
               :cl-glfw-opengl-version_1_1 
               :cl-glfw-opengl-version_1_2 
               :cowl-glfw 
               :iterate )
  :serial t
  :components 
  ((:file "package")
   (:file "yotta-zoomer")))
