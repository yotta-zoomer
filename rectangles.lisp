;;;; rectangles.lisp

(asdf:oos 'asdf:load-op 'cl-gd)

(defpackage #:rectangles
  (:use #:cl))

(in-package #:rectangles)


;; an abstract named rectangle thing
(defclass rectangle ()
  ((x :accessor x-of :iniform 0 :initarg :x)
   (y :accessor y-of :initform 0 :initarg :y)
   (w :accessor width-of :initform 1 :initarg :width)
   (h :accessor height-of :initform 1 :initarg :height)
   (name :accessof name-of :initform "Unknown"))

;; split one rectangle into two. Returns two rectangles,
;; the first of which is the given fraction of the original,
;; the second the remaining area of the original
(defmethod split ((self rectangle) fraction &key direction)
  (ccase direction
		 (:horizontal 
		  (let*
			  ((h-top (* (height-of rectangle) fraction))
			   (h-bot (- (height-of rectangle) h-top))
			   (split-y (+ h-top (y-of rectangle))))
			(values
			 (make-instance 'rectangle
							:x (x-of rectangle)
							:y (y-of rectangle)
							:width (width-of rectangle)
							:height h-top)
			 (make-instance 'rectangle
							:x (x-of rectangle)
							:y split-y
							:width (width-of rectangle)
							:height h-bot))))
		 (:vertical 
		  (let*
			  ((w-top (* (width-of rectangle) fraction))
			   (w-bot (- (width-of rectangle) w-top))
			   (split-x (+ w-top (x-of rectangle))))
			(values
			 (make-instance 'rectangle
							:x (x-of rectangle)
							:y (y-of rectangle)
							:width w-top
							:height (height-of rectangle))
			 (make-instance 'rectangle
							:x split-x
							:y (y-of rectangle)
							:width w-bot
							:height (height-of rectangle)))))))

;; Represents a chart of rectangles
(defclass chart ()
  ((rectangles :initform nil)
   (free-rectangle :initform (make-instance 'rectangle) :accessor free-rectangle-of)))
  

;; create a chart of rectangles from a property list of data
;; ( ( "Name" . value ) ...)

(defmethod make-instance ((self chart) &key data)
  (labels
	  ((sum-data (data)
				 "Return the sum od the data"
				 (reduce #'(lambda (t x) (+  t (cdr x))) data))
	   (normalise-data (sum data)
					   "Normalise the data."
					   (loop
						for element in data
						collect (cons (car element) (/ (cdr element) sum))))
	   (normalise (data)
				  "Normalise the data."
				  (normalise-data (sum-data data) data))
	   (split-for-datum (datum &key direction)
						(multiple-value-bind
							(taken free)
							(split (free-rectangle-of self) (cdr datum) :direction direction)
						  (progn
							(setf (free-rectangle-of self) free)
							(setf (name-of taken) (car data))
							(push (rectangles-of self) taken))))
	   (consume-data (data)
					 (when data
					   ((split-for-datum (car data) :horizontal)
						(consume-data (normalise (rest data))))))
	   (consume-data data))))
	   

		
		 
	   
(asdf:oos 'asdf:load-op 'cl-gd)

(in-package 'cl-gd)


(defun discretize (data range)
  (labels
	  ((sum-data (data)
				 (reduce #'+ data))))
  (let 
	  ((result (make-array (array-total-size data))))	
	(map-into result #'(lambda (x) (round (* (/ x (sum-data data))))) range)))
	

(defun region-sample (sample-count region sample-size sample-resolution sample-fn sample-score-fn)
  (labels
	  ((randomly-place main-region &key within)
	   (make-instance 'rectangle
					  :x (+ (x-of region) (random (- (width-of region) (width-of within))))
					  :y (+ (y-of region) (random (- (height-of region) (height-of within))))
					  :width (width-of within)
					  :height (height-of within))
	   (create-sample (region sample-resolution)
					  (let 
						  ((result (make-array (* sample-resolution sample-resolution)
											   :fill-pointer 0
											   :adjustable t)))
						(loop
						 for x = (x-of region) then (+ x (/ (width-of region) sample-resolution))
						 do
						 (loop
						  for y = (y-of region) then (+ y (/ (height-of region) sample-resolution))
						  (vector-push (funcall sample-fn x y))))
						result))
	   (let*
		   ((sample-region-rectangle 
			 (make-instance 'rectangle 
							:width (* (width-of region) sample-size)
							:height (* (width-of region) sample-size)))
			(samples
			 (loop
			  for sample-index from 0 below sample-count
							 collect
							 (progn
							   (create-sample
								(randomly-place sample-region-rectangle :within region)
								sample-resolution))))
			(sample-scores
			 (sort
			  (loop
			   for sample in samples
			   for sample-index = 0 then (1+ sample-index)
			   collect
			   (cons 
				(funcall sample-score-fun sample)
				sample-index))
			  #'< :key #'car)))
		 (loop
		  for score in sample-scores
		  (collect
		   (cons (car score) (nth (car score) samples))))))))
	   
		 

		




